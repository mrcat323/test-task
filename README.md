## Test task
Доброго времени суток. Для запуска и тестировании приложения необходимо сначала склонировать:
```shell
git clone https://gitlab.com/mrcat323/test-task
```
Далее необходимо скопировать нужные `.env` файлы и запустить через Docker:
```shell
cp .env.example .env
cd docker
cp .env.example .env
docker-compose up --build -d
```
Графическое управление базами данных **PHPMyAdmin** доступно по умолчанию по порту 8888

## Запуск тестов
Чтобы запустить тесты проекта необходимо быть в директории `docker`, далее запустить следующую команду:
```
docker-compose exec app php artisan test
```
## Все логи сохраняются в директории storage/logs как файл events-Y-m-d.log
## Руководство по роутам
## В качестве URL будет http://localhost:8081 (по-умолчанию)
## Регистрация
## POST /api/register
**Пример запроса:**
```
{
  "name": "Master Chief",
  "email": "masterchief@halo.com",
  "password": "password"
}
```
**Пример ответа:**
```json
{
    "success": true,
    "message": "User created successfully",
    "user": {
        "name": "Master Chief",
        "email": "masterchief@halo.com",
        "updated_at": "2024-06-14T12:59:35.000000Z",
        "created_at": "2024-06-14T12:59:35.000000Z",
        "id": 1
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODEvYXBpL3JlZ2lzdGVyIiwiaWF0IjoxNzE4MzY5OTc1LCJleHAiOjE3MTgzNzM1NzUsIm5iZiI6MTcxODM2OTk3NSwianRpIjoieTBybXVRTjV3cWNKczdSeSIsInN1YiI6IjEwIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.aFbLs7aHVzXQlppakI_VpNE22IRelK5brSnKvXxJtBc"
}
```
## Авторизация
### POST /api/login
**Пример запроса:**
```
{
  "email": "masterchief@halo.com",
  "password": "I need a weapon"
}
```
**Пример ответа:**
```json
{
    "success": true,
    "message": "Logged in successfully",
    "user": {
        "id": 1,
        "name": "Master Chief",
        "email": "masterchief@halo.com",
        "email_verified_at": null,
        "created_at": "2024-06-14T12:59:35.000000Z",
        "updated_at": "2024-06-14T12:59:35.000000Z"
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODEvYXBpL2xvZ2luIiwiaWF0IjoxNzE4MzcwMTc3LCJleHAiOjE3MTgzNzM3NzcsIm5iZiI6MTcxODM3MDE3NywianRpIjoiMmI4RHUzVVl2ZWV4bG9QdSIsInN1YiI6IjEwIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.CNEzXRiYj0qtR5zJbxGQHTFR97p8scy1UmYvTleUl8Y"
}
```
## Обновление токена
### GET /api/refresh
**В этот роут отправляеться только token через headers**
**Пример ответа**
```json
{
    "success": true,
    "message": "Token refreshed successfully",
    "user": {
        "id": 1,
        "name": "Master Chief",
        "email": "masterchief@halo.com",
        "email_verified_at": null,
        "created_at": "2024-06-14T12:59:35.000000Z",
        "updated_at": "2024-06-14T12:59:35.000000Z"
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODEvYXBpL3JlZnJlc2giLCJpYXQiOjE3MTgzNzAxNzcsImV4cCI6MTcxODM3NDAyNCwibmJmIjoxNzE4MzcwNDI0LCJqdGkiOiJON0tEQVg1RlV3S3c3NWFwIiwic3ViIjoiMTAiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.365sH__nj7Y8QpgE71WRvE8-D4lkc8rdonQYX5D_k8M"
}
```
## Выход
## GET /api/logout
**В этот роут отправляеться только token в headers**
**Пример ответа**
```json
{
    "success": true,
    "message": "Logged out successfully"
}
```
# Работа с заметками
## token для проверки авторизованности пользователя обязателен во всех роутах связанных с заметками
## Показать все заметки текущего пользователя
## GET /api/notes
**В этот роут отправляеться только token в headers**
**Пример ответа**
```json
{
    "success": true,
    "notes": [
        {
            "id": 17,
            "user_id": 1,
            "content": "Oops, what a mistake, let's fix it",
            "created_at": "2024-06-14T16:04:58.000000Z",
            "updated_at": "2024-06-14T16:04:58.000000Z"
        }
    ]
}
```
## Добавить заметку
## POST /api/notes/store
**Пример запроса**
```json
{
    "content": "This can be my second note in my 'diary'"
}
```
**Пример ответа**
```json
{
    "success": true,
    "note": {
        "content": "This can be my second note in my 'diary'",
        "user_id": 1,
        "updated_at": "2024-06-14T16:09:44.000000Z",
        "created_at": "2024-06-14T16:09:44.000000Z",
        "id": 28
    }
}
```
## Посмотреть заметку
## GET /api/notes/{id}
**Пример ответа**
```json
{
    "success": true,
    "note": {
        "id": 28,
        "user_id": 1,
        "content": "Oops, typo here",
        "created_at": "2024-06-14T16:09:44.000000Z",
        "updated_at": "2024-06-14T16:14:47.000000Z"
    }
}
```
**В случае если заметка не пренадлежит пользователю**
```json
{
    "success": false,
    "message": "Not Found"
}
```
## Редактировать заметку
## PATCH /api/notes/update
**Пример запроса**
```json
{
    "id": 28,
    "content": "Oops, typo here"
}
```
**Пример ответа**
```json
{
    "success": true,
    "note": {
        "id": 28,
        "user_id": 1,
        "content": "Oops, typo here",
        "created_at": "2024-06-14T16:09:44.000000Z",
        "updated_at": "2024-06-14T16:14:47.000000Z"
    }
}
```
**В случае если заметка не пренадлежит пользователю**
```json
{
    "success": false,
    "message": "Not Found"
}
```
## Удаление заметки
## DELETE /api/notes/delete
**Пример запроса**
```json
{
    "id": 28
}
```
**Пример ответа**
```json
{
    "success": true,
    "message": "Successfully deleted"
}
```
**В случае если заметка не найдено или не пренадлежит пользователю**
```json
{
    "success": false,
    "message": "Not Found"
}
```