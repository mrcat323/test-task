<?php

namespace Tests\Unit;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
// use PHPUnit\Framework\TestCase;

use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    // private $http;

    // protected function setUp(): void
    // {
    //     $this->http = new Client(['base_uri' => 'http://web/']);
    // }
    /**
     * A basic unit test example.
     */

    public function test_register(): void
    {
        $credentials = [
            'name' => 'Master Chief',
            'email' => 'masterchief@halo.com',
            'password' => 'I need a weapon'
        ];

        $this->json('post', '/api/register', $credentials)
            ->assertJsonStructure([
                'success',
                'message',
                'user',
                'token'
            ]);
    }

    public function test_login(): void
    {
        $credentials = [
            'email' => 'masterchief@halo.com',
            'password' => 'password'
        ];

        $user = User::create([
            'name' => 'Master Chief',
            'email' => 'masterchief@halo.com',
            'password' => Hash::make('password')
        ]);

        $this->json('post', '/api/login', $credentials)
            ->assertJsonStructure([
                'success',
                'message',
                'user',
                'token'
            ]);

        $this->assertDatabaseHas('users', $user->toArray());
    }

    public function test_refresh_token(): void
    {
        $user = User::factory()->create([
            'name' => 'Master Chief',
            'email' => 'masterchief@halo.com',
            'password' => Hash::make('password')
        ]);

        $token = Auth::login($user);

        $response = $this->json('get', '/api/refresh', headers: [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ]);

        $response->assertJsonStructure([
            'success',
            'message',
            'user',
            'token'
        ]);

        $this->assertNotEquals($token, $response->baseResponse->original['token']);
    }

    public function test_logout(): void
    {
        $user = User::create([
            'name' => 'Master Chief',
            'email' => 'masterchief@halo.com',
            'password' => Hash::make('password')
        ]);

        $token = Auth::login($user);

        $this->json('get', '/api/logout', headers: [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertExactJson([
            'success' => true,
            'message' => 'Logged out successfully'
        ]);
    }
}
