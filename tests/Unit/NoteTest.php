<?php

namespace Tests\Unit;

use App\Models\Note;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class NoteTest extends TestCase
{
    use RefreshDatabase;

    public function test_retrieve_all(): void
    {
        $user = User::factory()->create();

        Note::factory(5)->create([
            'user_id' => $user->id
        ]);

        $token = Auth::login($user);

        $this->json('get', '/api/notes', headers: [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'success',
            'notes'
        ])
        ->assertJsonCount(5, 'notes');
    }

    public function test_show_certain_note(): void
    {
        $user = User::factory()->create();

        $note = Note::factory()->create([
            'user_id' => $user->id
        ]);

        $token = Auth::login($user);

        $this->json('get', '/api/notes/' . $note->id, headers: [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'success',
            'note'
        ])->assertJson([
            'success' => true,
            'note' => $note->toArray()
        ]);
    }

    public function test_store_note(): void
    {
        $user = User::factory()->create();

        $token = Auth::login($user);

        $data = [
            'content' => 'This is my first note, hooray'
        ];

        $this->json('post', '/api/notes/store', $data, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'success',
            'note'
        ]);

        $this->assertDatabaseHas('notes', $data);
    }

    public function test_update_note(): void
    {
        $user = User::factory()->create();

        $note = Note::factory()->create([
            'user_id' => $user->id
        ]);

        $token = Auth::login($user);

        $data = [
            'id' => $note->id,
            'content' => 'Oops, what a mistake, let\'s fix it'
        ];

        $this->json('patch', '/api/notes/update', $data, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'success',
            'note'
        ]);

        $this->assertDatabaseHas('notes', $data);
    }

    public function test_delete_note(): void
    {
        $user = User::factory()->create();

        $note = Note::factory()->create([
            'user_id' => $user->id
        ]);

        $token = Auth::login($user);

        $data = ['id' => $note->id];

        $this->json('delete', '/api/notes/delete', $data, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ])->assertJsonStructure([
            'success',
            'message'
        ]);

        $this->assertDatabaseMissing('notes', $data);
    }

}
