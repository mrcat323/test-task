<?php

namespace App\Services;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger as MonologLogger;

class Logger
{
    public static function events()
    {
        $logger = new MonologLogger('events');
        $filePath = storage_path('logs/events-' . date('Y-m-d') . '.log');
        touch($filePath);
        $handler = new StreamHandler($filePath, Level::Info);
        $logger->pushHandler($handler);

        return $logger;
    }
}