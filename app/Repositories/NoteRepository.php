<?php

namespace App\Repositories;

use App\Contracts\NoteInterface;
use App\Models\Note;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class NoteRepository implements NoteInterface
{
    public function list(): Collection
    {
        return Auth::user()->notes;
    }

    public function exists(int $id): bool
    {
        return Auth::user()->notes()->where('id', $id)->exists();
    }

    public function find(int $id): Note|array
    {
        return Auth::user()->notes()->where('id', $id)->first();
    }

    public function store(array $data): Note
    {
        return Auth::user()->notes()->create($data);
    }

    public function update(int $id, array $data): Note
    {
        $note = $this->find($id);

        $note->update($data);

        return $note;
    }

    public function destroy(int $id): void
    {
        $note = $this->find($id);

        $note->delete();
    }
}