<?php

namespace App\Contracts;
use App\Models\Note;
use Illuminate\Support\Collection;

interface NoteInterface
{
    public function list(): Collection;

    public function find(int $id): Note|array;

    public function exists(int $id): bool;

    public function store(array $data): Note;

    public function update(int $id, array $data): Note;

    public function destroy(int $id): void;
}