<?php

namespace App\Http\Controllers;

use App\Contracts\NoteInterface;
use App\Http\Requests\Note\Create as CreateNote;
use App\Http\Requests\Note\Update as UpdateNote;
use App\Http\Requests\Note\Delete as DeleteNote;
use App\Services\Logger;

class NoteController extends Controller
{
    public function __construct(private NoteInterface $note, private Logger $logger)
    {
        // 
    }

    public function index()
    {
        $this->logger->events()->info('Retrieving notes');
        return response()->json([
            'success' => true,
            'notes' => $this->note->list()
        ]);
    }

    public function show($id)
    {
        if (!$this->note->exists($id)) {
            $this->logger->events()->info('Could not find note of id=' . $id);
            return response()->json([
                'success' => false,
                'message' => 'Not Found'
            ], 404);
        }
        $this->logger->events()->info('Could find note of id=' . $id);
        $note = $this->note->find($id);

        return response()->json([
            'success' => (bool) $note,
            'note' => $note
        ]);
    }

    public function store(CreateNote $request)
    {
        $this->logger->events()->info('Storing with: ' . json_encode($request->all()));
        $note = $this->note->store($request->all());

        return response()->json([
            'success' => true,
            'note' => $note
        ]);
    }

    public function update(UpdateNote $request)
    {
        if (!$this->note->exists($request->id)) {
            $this->logger->events()->info('Could not find note of id=' . $request->id);
            return response()->json([
                'success' => false,
                'message' => 'Not Found'
            ], 404);
        }
        $this->logger->events()->info('Updating id=' . $request->id . ' with: ' . json_encode($request->all()));

        $this->note->update($request->id, $request->only('content'));

        $note = $this->note->find($request->id);

        return response()->json([
            'success' => true,
            'note' => $note
        ]);
    }

    public function destroy(DeleteNote $request)
    {
        if (!$this->note->exists($request->id)) {
            $this->logger->events()->info('Could not find note of id=' . $request->id);
            return response()->json([
                'success' => false,
                'message' => 'Not Found'
            ], 404);
        }

        $this->logger->events()->info('Delete note of id=' . $request->id);
        $this->note->destroy($request->id);

        return response()->json([
            'success' => true,
            'message' => 'Successfully deleted'
        ]);
    }
}
