<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\Login;
use App\Http\Requests\Auth\Register;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Register $request)
    {
        $user = User::create([
            ...$request->only(['name', 'email']),
            'password' => Hash::make($request->password)
        ]);

        $token = Auth::login($user);

        return response()->json([
            'success' => true,
            'message' => 'User created successfully',
            'user' => $user,
            'token' => $token
        ]);
    }

    public function login(Login $request)
    {
        $token = Auth::attempt($request->only(['email', 'password']));
        if (!$token) {
            return response()->json([
                'success' => false,
                'message' => 'Unauthorized'
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Logged in successfully',
            'user' => Auth::user(),
            'token' => $token
        ]);
    }

    public function refresh()
    {
        return response()->json([
            'success' => true,
            'message' => 'Token refreshed successfully',
            'user' => Auth::user(),
            'token' => Auth::refresh()
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return response()->json([
            'success' => true,
            'message' => 'Logged out successfully'
        ]);
    }
}
