#!/bin/bash

set -e

chown -Rv www-data:www-data /var/www/html/storage/
composer install && \
php artisan cache:clear && \
php artisan migrate --force && \
php-fpm